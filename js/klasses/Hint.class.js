Zipzing.Hint = Class.create({

  /********************************************************************************************
   * Constructor
   *******************************************************************************************/
  initialize : function(ctorData) {
    //this.element = ctorData.element || this.initDOMElement(ctorData.hint);
    
    // Hint Options
    this.options = Object.extend({
      text     : "",
      position : "left"
    }, ctorData.options || {});
    this.initOptions();
  },
  
  
  /********************************************************************************************
   * initialize() Helpers
   *******************************************************************************************/
  initDOMElement : function(h) {
    // @todo: Builder.node ...
  },
  
  initOptions : function() {
    if (this.options.text) this.setText(this.options.text);
    if (this.options.position) this.setPosition(this.options.position);
  },
  
  
  /********************************************************************************************
   * Setters
   *******************************************************************************************/
  setText: function(text){
    this.options.text = text;
    //@todo: paint
  },
  
  setPosition: function(position) {
    this.options.position = position;
    //@todo: paint
  }
});
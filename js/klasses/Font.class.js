Zipzing.Font = Class.create({

  /********************************************************************************************
   * Constructor
   *******************************************************************************************/
  initialize : function(ctorData) {    
    //this.element = ctorData.element || this.initDOMElement(ctorData.font);
    
    // Font Options
    this.options = Object.extend({
      family : "arial",
      size   : "12px",
      color  : "000000"
    }, ctorData.options || {});
    this.initOptions();
  },
  
  
  /********************************************************************************************
   * initialize() Helpers
   *******************************************************************************************/
  initDOMElement : function(f) {
    //var DNA = $('DNA_Font');
    // Builder.node ...
  },
  
  initOptions : function() {
    if (this.options.family) this.setFamily(this.options.family);
    if (this.options.size) this.setSize(this.options.size);
    if (this.options.color) this.setColor(this.options.color);
  },
  
  
  /********************************************************************************************
   * Setters
   *******************************************************************************************/
  setFamily : function(family) {
    this.options.family = family;
    // paint:
    //this.element.style.fontFamily = family;
  },
  
  setSize : function(size) {
    this.options.size = size;
    // paint:
    //this.element.style.fontSize = size;
  },
  
  setColor : function(color) {
    this.options.color = color;
    // paint:
    //this.element.style.color = color;
  }
});
var Zipzing = Zipzing || {};
//
Zipzing.Panel = Class.create({
  /********************************************************************************************
   * Constructor
   *******************************************************************************************/
  initialize : function(ctorData) {
    this.Type = 'Zipzing.Panel';
    
    this.Page = ctorData.page;
    this.Survey = ctorData.survey;
    this.element = ctorData.element || this.initDOMElement(ctorData.panel, $('DNA_Panel'));
    
    this.Header = ctorData.header ? new Zipzing.PanelHeader(ctorData.header) : "Panel Title";
    
    // Panel Options
    this.options = Object.extend({
      panelID : ctorData.panel.panelID,//this.element.id,
      bgcolor : "#fff",
      margin  : "0px",
      border  : new Zipzing.Border(ctorData.border || {}/*{weight:'1px',color:'blue'}*/)
      //header  : new Zipzing.PanelHeading(ctorData.header || {}/*, {options}*/),
      //zindex  : 1000,
    }, ctorData.panel.options || {});
    this.initOptions();
    
    // Panel Css
    this.css = Object.extend({
      // default css properties (?)
    }, ctorData.panel.css || {});
    
    // Panel Coordinates
    this.coords = Object.extend({
      x:0, 
      y:0
    }, ctorData.panel.coords || {});
    
    // Panel Behaviors
    this.fx = Object.extend({
      autosize: false, // if autosize is active, it messes up heights making resizable clumsy! one or the other.
      draggable: true,
      dragHnd: null,//this.element.down('h3'),
      droppable: true,
      resizable: true,
      resizeHnd: null,
      resize_grippies: [
        'bm', //bottom-middle
        'br', //bottom-right
        'mr'  //middle-right
      ],
      sortable: true,
      zindex: null  
    }, ctorData.panel.behaviors || {/*empty*/});
    this.paint();
    zip.fx(this);
    
    this.properties = {
      io: {
        loadURL : '/IMA5/?method=cSurveyProperties.displayPanelProperties',
        loadCB  : this.loadPropertiesCallback.bind(this),
        saveURL : '/IMA5/?method=cSurveyProperties.savePanelProperties', // TODO!
        saveCB  : this.save.bind({pointcut:'after'})
      },
      hash : {
        // starts out empty
      },
      observers: [
        {elm:'panelHeaderYesNo',    evt:'change', fn:this.setHeaderYesNo.bindAsEventListener(this)},
        {elm:'panelHeaderText',     evt:'keyup',  fn:this.setHeaderText.bindAsEventListener(this)},
        {elm:'panelHeaderFont',     evt:'change', fn:this.setHeaderFont.bindAsEventListener(this)},
        {elm:'panelHeaderFontSize', evt:'change', fn:this.setHeaderFontSize.bindAsEventListener(this)},
        {elm:'panelHeaderVisible',  evt:'change', fn:this.setHeaderVisible.bindAsEventListener(this)},
        {elm:'panelMarkupDiv',      evt:'change', fn:this.setDOMElement.bindAsEventListener(this, 'div')},
        {elm:'panelMarkupFieldset', evt:'change', fn:this.setDOMElement.bindAsEventListener(this, 'fieldset')}
      ]
    };
    
    //this.observers = Object.extend({/*defaults*/}, ctorData.observers || {/*empty*/});
    this.initObservers();
    
    // Panel Rows
    this._Rows = ctorData.rows || [];
    this.initRows(this);
    
    this.focus();
  },
  
  /********************************************************************************************
   * initialize() Helpers
   *******************************************************************************************/
  initDOMElement : function(panel, DNA) {
    return $(Builder.node(DNA.tagName,{
      id        : panel.key,
      style     : 'display:none;',
      className : 'surveyPanel'
    })).update(DNA.innerHTML || DNA.innerText);
  },

  initOptions : function() {
    /*
    if (this.options.panelID) this.setPanelID(this.options.panelID);
    if (this.options.rows) this.setRows(this.options.rows);
    if (this.options.bgcolor) this.setBGColor(this.options.bgcolor);
    if (this.options.margin) this.setMargin(this.options.margin);
    //if (this.options.border) this.setBorder(this.options.border);
    //if (this.options.heading) this.setPanelHeading(this.options.heading);
    if (this.Header) this.setHeader(this.Header);
    */
  },
  
  initObservers : function() {
    this.element.observe('mousedown', this.focus.bind(this));
    this.element.observe('hover', function(e) {e.stop();});
    //this.element.observe('mousemove', this.mouseMove.bindAsEventListener(this));
  },
  
  initRows : function(_this) {
    this.Rows = [];
    this._Rows.each(function( _row ) {
      _this.Rows.push(new Zipzing.Row( _row ));
    });
    delete this._Rows;
  },
  
  /********************************************************************************************
   * Alignment Challenge: TK-02511
   *******************************************************************************************/
  deferOps : function(_this) {
    _this.labelWidths();
  },
  
  labelWidths : function() {
    if(zip.debug) console.log("@ %o.alignQuestions()", this);
    var w=0, pad=0, _max=0, labels = this.questionLabelsByCol(0), _ptr=0;
    //console.log("left most labels (%s) = %o", labels.length, labels);
    
    //1)determine widest label
    labels.each(function(lbl, idx) {
      w = lbl.getWidth();
      _max = (w > _max) ? w : _max;
      _ptr = idx+1; console.log("lables.length(%s) ?= _ptr(%s)",labels.length,_ptr);
    });
    console.log("max label width = %s px",_max);
    
    this.alignQuestions(labels, _max);
  },
  
  alignQuestions : function(labels, maxW) {
    // this operation should be safe & idempotent (running it 10x should be no different than running it once), 
    // but it is not [yet] and... ugh.
    labels.each(function(lbl) {
      var lblW = lbl.getWidth();
      if (maxW != lblW) {
        pad = maxW - lblW;
        lbl.setStyle({ paddingRight:pad+'px' });
      }
    });
  },
  
  questionLabelsByCol : function(colIndex) {
    var _labels = [];
    this.element.select('div.surveyRow').each(function(row) {
      _labels.push(row.down('div.questionLabel', colIndex));
    });
    return _labels;
  },
  
  justifyLabels : function() {
    labels = $$('.surveyPanel .questionLabel label');
    labels.each(function(lbl) {
      lbl.setStyle({
        'float':'right'
      });
    });
  },
  
  /********************************************************************************************
   * IO (ajax) callbacks n such
   *******************************************************************************************/
  register : function(what) {
    //if(zip.debug) console.log('@ %s.register( %s )', this.element.id, what);
    switch(what) {
      case 'sortables':
        zip.fx.lib.sort(this);//this.makeSortable();
        zip.fx.lib.drop(this);//this.makeDroppable();
        break;
    };
    
    //DEBUG
    this.Rows.each(function(row) {
      //if(zip.debug) console.log(" --> verifying %s.Panel: %s", row.element.id, row.Panel.element.id);
    });
  },
  
  load : function() {},
  
  loadProperties : function() {
    this.Survey.io(this, 'get', this.properties.io.loadURL, this.properties.io.loadCB);
  },
  
  loadPropertiesCallback : function() {
    // 1) use properties hash to populate properties form:
    $H(this.properties.hash).each(function(p) {
      $(p[0].toString()).value = p[1];
    });
    // 2) attach properties form event observers:
    this.properties.observers.each(function(obs) { $(obs.elm).observe(obs.evt, obs.fn); });
  },
  
  //TODO:SAVING -- next sprint!
  save : function(args) {},
  saveProperties : function() {},
  savePropertiesCallback : function() {},
  
  _getValue : function(args) {
    return Try.these(
      function() { return args[0].element().checked; },
      function() { return args[0].element().value; },
      function() { return args[0]; }
    ) || 'undetermined value';
  },
  
  /********************************************************************************************
   * Setters:
   * NOTE: the args passed to setters are purposely *not* made explicit,
   *  because setters may receive either an {event object} or 'vanilla value'
   *******************************************************************************************/ 
  setHeader : function() {},
  
  setHeaderYesNo : function() {
    var chk = $('panelHeaderYesNo');
    this.element.down('h3').setStyle({ 
      display: (this.properties.hash.panelHeaderYesNo = chk.checked) ? 'inline' : 'none' 
    });
    chk.up('tr').nextSiblings().each(function(prop) {
      if (prop.hasClassName('heading')) chk.checked ? prop.appear() : prop.fade();
    });
  },
  
  setHeaderText : function() {
    this.element.down('h3').update(
      this.properties.hash.panelHeaderText = this._getValue(arguments)
    );
  },
  
  setHeaderFont : function() {
    this.element.down('h3').setStyle({
      fontFamily: this.properties.hash.panelHeaderFont = this._getValue(arguments)
    });
  },
  
  setHeaderFontSize : function() {
    this.element.down('h3').setStyle({
      fontSize: this.properties.hash.panelHeaderFontSize = this._getValue(arguments)
    });
  },
  
  setHeaderVisible : function() {
    this.element.down('h3').setStyle({
      visibility: this.properties.hash.panelHeaderVisible = this._getValue(arguments)
    });
  },
  
  setPanelID: function() {
    this.properties.hash.panelID = this._getValue(arguments);
  },
  
  setDOMElement : function() {
    var _value = this._getValue(arguments);
  },
  
  /*setPanelMarkup : function() {
    var elm = Builder.node(tag,{
      id        : this.element.id,
      className : 'surveyPanel'
    }).update(this.element.innerHTML);
    this.element.replace(elm);
    elm.down('.panelHeader').replace(
      Builder.node(hdr,{className:'panelHeader'}).update(this.Header)
    );
    
    elm.setStyle({
      top    : this.element.style.top,
      left   : this.element.style.left,
      width  : this.element.style.width,
      height : this.element.style.height
    });
    this.element = elm;
    zip.fx(this);
  },*/
  
  setRows: function() {
    this.Rows = rows;
  },
  
  setBGColor: function() {
    this.options.bgcolor = bgcolor;
  },
  
  setMargin: function() {
    this.options.margin = margin;
  },
  
  /********************************************************************************************
   * zip.fx library callbacks:
   *******************************************************************************************/
  dragCallback : function() {
    //this.dumpCoords(); //this.dumpOffsets();
  },
  
  dragStartCallback : function(drag, evt) {
    //this.element.style.zindex = ++this.options.zindex;
  },
  
  hoverCallback : function(drag, drop, evt) {
    if (drag.hasClassName('toolboxPanel')) {
      // no kinky panel-on-panel stuff allowed... //if(zip.debug) console.log('no drop here... move along.');
      //TODO: display "not allowed" icon of some sort
    }
  },
  
  dropCallback : function(drag, drop, evt) {
    if (drag.hasClassName('widget') && !drag.hasClassName('toolboxPanel')) this.addRow(drag, drop, evt, this);
    //if(zip.debug) console.log("[ASSERT] @ %s.dropCallback(drag:%o, drop:%o)", this.element.id, drag, drop);
    
    this.deferOps.defer(this);
  },
 
  resizeEndCallback : function() {
    //if(zip.debug) console.log("@ %s.resizeEndCallback(%o)",this.element.id, arguments);
  },
  
  sortUpdateCallback : function(sort) {
    if (this.Survey.Registry._DraggedRow && this.Rows.indexOf(this.Survey.Registry._DraggedRow) == -1) {
      // re-assignment processing:
      var DraggedFrom = this.Survey.Registry._DraggedRow.Panel;
      // leveraging Prototype's "Sortable.sequence" ...
      var row_sequence_id = this.Survey.Registry._DraggedRow.element.id.split('_')[1];
      var sortPos = Sortable.sequence(this.element.id).indexOf(row_sequence_id);
      var oldPos = DraggedFrom.Rows.indexOf(this.Survey.Registry._DraggedRow);
      
      // splice out:
      var splicedRow = DraggedFrom.Rows.splice(oldPos, /*delete*/1);
      
      // splice in, complete re-assignment:
      this.Rows.splice(sortPos, /*delete*/0, /*insert*/splicedRow[0]);
      this.Survey.Registry._DraggedRow.Panel = this;
      
      // "unregister":
      delete this.Survey.Registry._DraggedRow;
      
      // TODO: this is not optimal! only need to resize the affected panels, but *after* row re-assignment is complete
      this.Page.Panels.each(function(panel) { 
        if(zip.debug) console.log(" ----> DUMP of %s's .Rows: %o", panel.element.id, panel.Rows);
        panel.autosize();
      });
    }
    //else { if(zip.debug) console.log(" --[ASSERT]--> ._DraggedRow has been un-registered; no more processing to do."); }

    this.deferOps.defer(this, alignQuestions);
  },
  
  /********************************************************************************************
   * add/remove/etc:
   *******************************************************************************************/
  addRow : function(drag, drop, evt, _this) {
    if(zip.debug) console.log("@ %s.addRow(drag:%o, drop:%o)", this.element.id, drag, drop);
    var row_sequence_id = drop.id.split('_')[1];
    var sortPos = Sortable.sequence(this.element.id).indexOf(row_sequence_id);
    this.Rows.splice(sortPos, /*delete*/0, /*insert*/_row = new Zipzing.Row(_this.rowTemplate()));
    // Chaining the Callback: rows are transparent, now build the actual widget
    _row.dropCallback(drag, drop, evt);
    
    this.Page.register('sortables');//TODO: Survey.extensions.js > Registry mixin object
  },
  
  rowTemplate : function() {
    var rowCount = this.Rows.length + 1;
    return {
      // ancestry:
      survey    : this.Survey,
      panel     : this,
      // attributes:
      key       : 'row-' + rowCount,
      rowID     : rowCount,
      options   : {},
      behaviors : {},
      observers : {}
    };
  },
  
  removeRow : function() {
    
  },
  
  /*reAssignRow : function() {
    
  },*/
  
  /********************************************************************************************
   * GUI Utilities & FX!
   *******************************************************************************************/
  paint : function() {
    $(this.Page.element).insert(this.element); 
    this.element.setStyle({
      left : this.coords.x + 'px',
      top  : this.coords.y + 'px'
    });
    this.element.appear(zip.fx.lib.transitions);
    
    ///*DEBUG/if(Zipzing.DEBUG)*/ this.element.insert('<span class="offsets">x,y</span>',{position:'after'});
    /*
    var _id = this.element.id+"_debugger";
    this._debugger = Builder.node(this.element.tagName,{
      id        : _id,
      className : "debugger"
    }).update("_debugger");
    
    var _ht = this.element.getHeight();
    
    this.element.insert(this._debugger);
    this._debugger.setStyle({ marginTop:_ht+'px',left:_ht+'px' });
    //this._debugger.clonePosition(this._debugger,this.element);
    */
  },
  
  erase : function() {
    this.element./*fade or*/puff(zip.fx.lib.transitions);
  },
  
  warn : function() {
    this.element.pulsate(zip.fx.lib.errPulse);
  },
  
  focus : function() {
    //if(zip.debug) console.log("--> document.activeElement = %o", document.activeElement);
    this.loadProperties();
    //this.register('observers');
    this.element.setStyle({
      zIndex : ++this.Survey.Registry._zindex
    });
  },  
  
  dumpCoords : function() {},
  dumpOffsets : function() {
    // .cumulativeOffset()
    var cOffsets = this.element.cumulativeOffset();
    // .positionOffset()
    var pOffsets = this.element.positionedOffset();
    
    if (this.element.down('span.offsets')) this.element.down('span.offsets').update(
      '#offsetParent(CSS Containing Block): <strong>'+this.element.getOffsetParent().id+'</strong><br />'+
      'position.offsetLeft: <strong>'+pOffsets.left+'px</strong>'+
      ',  position.offsetTop: <strong>'+pOffsets.top+'px</strong><br />'+
      'cumulative.offsetLeft: <strong>'+cOffsets.left+'px</strong>'+
      ',  cumulative.offsetTop: <strong>'+cOffsets.top+'px</strong>'
    );
  },
  
  autosize : function(deltaHt) {
    if (!this.fx.autosize) return;
    //
    var _ht = 0;
    if (true) _ht += this.element.down('h3').getHeight();
    try {
      this.Rows.each(function(row) {
        _ht += row.element.getHeight();
        _ht += 20;
        //if(zip.debug) console.log("cumulative row height: "+_ht);
      })  
    }
    catch(e) { _ht += 20; }
    this.element.morph('height:' + /*(this.element.getHeight() + deltaHt)*/_ht +'px', zip.fx.lib.transitions);
  },
  
  lock : function() {
    // show locked padlock icon
    // undo Draggable behavior
    this.Drag.destroy();
    //this.Resize.destroy(); // undo Resizable behavior
    // TODO
  },
  
  unlock : function() {
    if (this.fx.draggable) this.makeDraggable();
    //if (this.behaviors.resizable) this.makeResizable();
    //show unlocked padlock icon
  }
});
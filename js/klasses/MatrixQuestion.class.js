var Zipzing = Zipzing || {};
/**
 * @Class MatrixQuestion
 *  inherits from Question...
 *
 *  Question
 *    MatrixQuestion
 */
Zipzing.MatrixQuestion = Class.create(
 Zipzing.Question, {
  /********************************************************************************************
   * Constructor
   *******************************************************************************************/
  initialize : function($super, ctorData) {
    $super(ctorData);
    
    this.options = Object.extend({/*default(s)*/}, ctorData.options || {/*empty*/});
    //this.fx = Object.extend({/*defaults*/}, ctorData.fx || {/*empty*/});
    
    this.properties = {
      io: {
        loadURL : '^/matrix_question/%id%/properties', //fixture?
        loadCB  : this.register.bind(this, 'properties'), // cache n curry
        saveURL : '^/matrix_question/%id%/save', // save action
        saveCB  : this.save.bind({pointcut:'after'}) // a little AOP for good measure =)
      },
      observers: this.properties.observers.concat([//inheritance-wise, this is a top-down array merge:
      /*
        {elm:'',  evt:'change',  fn:this. .bindAsEventListener(this)},
        {elm:'',  evt:'keyup',   fn:this. .bindAsEventListener(this)},
        {elm:'',  evt:'change',  fn:this. .bindAsEventListener(this)},
        {elm:'',  evt:'change',  fn:this. .bindAsEventListener(this)},
        {elm:'',  evt:'change',  fn:this. .bindAsEventListener(this)},
      */
      ]),
      hash : {}
    };
    
    // Matrix Rows
    this._Rows = ctorData.rows || [];
    this.initRows(this);
    
    // Matrix Columns
    // @todo: construct via MatrixColumns.class.js
    this._Columns = ctorData.columns || [];
    this.initColumns(this);
    
    this.focus();
  },
  
  /********************************************************************************************
   * initialize() Helpers
   *******************************************************************************************/
  //initOptions : function() {},
  //initFx : function() {},
  
  initRows : function(_this){
    this.Rows = [];
    this._Rows.each(function(row){
      /*
      _this.Rows.push(new Zipzing.Row({
        
      });
      */
    });
  },
  
  initColumns : function(_this) {
    this.Columns = [];
    this._Columns.each(function(col){
      /*
      _this.Columns.push(new Zipzing.MatrixColumn({
        matrix : _this,
        column : col
      });
      */
    });
  },
  
  /********************************************************************************************
   * Ajax callback(s)
   *******************************************************************************************/
  register : function() {
    if(zip.debug) console.log("@%s.register()", this.element.id);
  },
  
  /********************************************************************************************
   * Setters
   *******************************************************************************************/
  setRows : function(rows) {
    this.Rows = rows;
  },
  
  /********************************************************************************************
   * add/remove/etc
   *******************************************************************************************/
  addRow :  function() {
    ;
  },
  
  removeRow :  function() {
    ;
  },
  
  addColumn : function() {
    ;
  },
  
  removeColumn : function() {
    ;
  }
});
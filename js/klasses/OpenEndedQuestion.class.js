var Zipzing = Zipzing || {};
/**
 * @class OpenEndedQuestion
 *  class def for a Survey *Open Ended* Question...
 *
 * @superclass Question
 *  inherits from Zipzing.Question
 */
Zipzing.OpenEndedQuestion = Class.create(
  Zipzing.Question, {
  /********************************************************************************************
   * Constructor
   *******************************************************************************************/
  initialize : function($super, ctorData) {
    $super(ctorData);
    
    //this.options = Object.extend({/*defaults*/}, ctorData.options || {/*empty*/});
    //this.options = Object.extend({/*defaults*/}, ctorData.behaviors || {/*empty*/});
  }
  
  /********************************************************************************************
   * initialize() Helpers
   *******************************************************************************************/
   
  /********************************************************************************************
   * Setters + Event Handlers
   *******************************************************************************************/
});
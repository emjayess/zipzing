Zipzing.Content = Class.create({

  initialize : function(ctorData){
    this.Row = ctorData.row;
    this.Zipzing = ctorData.zipzing;
    this.element = ctorData.element || this.initDOMElement(ctorData.content);
    this.contentString = ctorData.contentstring || '<em>sample</em> <strong>content</strong> [<a href="#" style="font-size:75%;">edit</a>]';

    this.options = Object.extend({
      contentID  : null,
      tempID     : null,
      mfieldID   : null,
      //@@todo:remove//fuseaction : 'displayContentProperties',
      propsURL   : '/web/path/to/contentproperties'//a json store e.g. couchdb document
    }, ctorData.options || {/*empty*/});
    this.initOptions();

    this.behaviors = Object.extend({
      FxDuration : {duration:0.5}
    }, ctorData.behaviors || {/*empty*/});

    this.paint();
    this.initBehaviors();
    //this.focus();
  },

  initDOMElement : function(c){
    var DNA = $('DNA_Content');
    return $(Builder.node(DNA.tagName,{
      id        : c.contentID,
      style     : 'display:none;',
      className : 'ZipzingContent'
    })).update(DNA.innerHTML || DNA.innerText);
  },

  initOptions : function(){
    if (this.options.contentID) this.setContentID(this.options.contentID);
    if (this.options.tempID) this.setTempID(this.options.tempID);
    if (this.options.mfieldID) this.setMfieldID(this.options.mfieldID);
    //@@todo //if (this.contentString) this.setContent(this.contentString);
  },

  initBehaviors : function(){
    //@@todo
  },

  setContentID : function(id){
    this.options.contentID = id;
  },

  setTempID : function(id){
    this.options.tempID = id;
  },

  setMfieldID: function(id){
    this.options.mfieldID = id;
  },

  setContent : function(contentstring){
    this.contentString = contentstring;
    //paint
    this.element.update(contentstring);
  },

  paint : function() {
    this.Row.element.insert(this.element);
    this.Row.autosize(this.element.getHeight());
    new Effect.Appear(this.element, this.behaviors.FxDuration);
  },

  focus : function() {
    this.Zipzing.actor = this;
    this.Zipzing.loadProperties(this.options.propsURL);
  },

  lock : function() {
    //@@todo
  },

  unlock : function() {
    //@@todo
  },

  contemplate : function() {
    //@@todo
  },

  edit : function() {
    //@@todo launch WYSIWYG for this.contentString (?)
  },

  //update : function() {},
  wysiwyg_update : function(){
    //@@todo receive & reflect content updates from TinyMCE
  },

  save : function() {
    //@@todo persist to json store e.g. couchdb document
    //@@todo this.properties.io (ajax or websocket behaviors)
  }
});
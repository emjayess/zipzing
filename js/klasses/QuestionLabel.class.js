Zipzing.QuestionLabel = Class.create({

  /********************************************************************************************
   * Constructor
   *******************************************************************************************/
  initialize : function(ctorData) {
    this.element = ctorData.element || this.initDOMElement(ctorData.qlabel);
    
    // QuestionLabel Options
    this.options = Object.extend({
      text      : "",
      visible   : true,
      position  : "left",
      alignment : "left",
      font      : new Zipzing.Font(elm_id /*, {options}*/)
    }, ctorData.options || {});
    this.initOptions();
    
    // QuestionLabel Behaviors
    //this.behaviors = Object.extend({/*defaults*/},/*ctorData.behaviors*/arguments[2] || {/*empty*/});
    //this.initBehaviors();
  },
  
  
  /********************************************************************************************
   * initialize() Helpers
   *******************************************************************************************/
  initDOMElement : function(lbl) {
    // Builder.node ...
  },
  
  initOptions : function() {
    if (this.options.text) this.setText(this.options.text);
    if (this.options.visible) this.setVisibility(this.options.visible);
    if (this.options.position) this.setPosition(this.options.position);
    if (this.options.alignment) this.setAlignment(this.options.alignment);
  },
  
  initBehaviors : function() {},
  
  
  /********************************************************************************************
   * Setters
   *******************************************************************************************/
  setText : function(text) {
    this.options.text = text;
    this.element.down('label').update(text);
  },
  
  setVisibility : function(visible) {
    this.options.visible = visible;
    // use CSS 'visibility'...
    this.element.style.visibility = visible ? 'visible' : 'hidden';
    // or CSS 'display'...
    // this.element.style.display = isVisible ? 'normal' : 'none';
  },
  
  setPosition : function(position) {
    this.options.position = position;
    //this.element.style.display = (position=='above') ?  'block' : /*left?*/'inline';
  },
  
  setAlignment : function(alignment) {
    this.options.alignment = alignment;
    //this.element.style.textAlign = alignment; // ?
  }
});
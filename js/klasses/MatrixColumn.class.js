Zipzing.MatrixColumn = Class.create({

  /********************************************************************************************
   * Constructor
   *******************************************************************************************/
  initialize : function(ctorData) {
    //this.element = ctorData.element || this.initDOMElement(ctorData.matrixcolumn);
    
    // MatrixColumn Options
    this.options = Object.extend({
      weight : 0,
      label  : ""
    }, ctorData.options || {});
    this.initOptions();
    
    // MatrixColumn Behaviors
    this.behaviors = Object.extend({/*defaults*/}, ctorData.behaviors || {/*empty*/});
    this.initBehaviors();
  },
  
  
  /********************************************************************************************
   * initialize() Helpers
   *******************************************************************************************/
  initDOMElement : function(col) {
    // Builder.node ...
  },
  
  initOptions : function() {
    if(this.options.weight) this.setWeight(this.options.weight);
    if(this.options.label) this.setLabel(this.options.label);
  },
  
  initBehaviors : function() {},
  
  
  /********************************************************************************************
   * Setters
   *******************************************************************************************/
  setWeight : function(weight) {
    this.options.weight= weight;
    //@todo: paint
  },
  
  setLabel : function(label) {
    this.options.label = label;
    //@todo: paint
  }
});
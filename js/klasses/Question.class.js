var Zipzing = Zipzing || {};
/**
 * @class Question
 *  class def for a Survey Question...
 *  This class won't be used directly, only as a base class ($super) for classical inheritance by specific types of questions
 */
Zipzing.Question = Class.create({
  /********************************************************************************************
   * Constructor
   *******************************************************************************************/
  initialize : function(ctorData) {
    //if(zip.debug) console.log("constructing Question object; ctorData.element = %o", ctorData.element);
    this.Row = ctorData.row;
    this.Survey = ctorData.survey;
    this.element = ctorData.element || this.initDOMElement(ctorData);
    
    // Question Options
    this.options = Object.extend({
      questionID : null,
      tempID     : null,
      label      : ctorData.label, //new Zipzing.QuestionLabel(elm_id, {text:arguments[1].label} || {}),
      hint       : 'dummy hint', //new Zipzing.Hint(elm_id),
      mfieldID   : null,
      validation : 'dummy validation' //new Zipzing.QuestionValidation(elm_id) 
    }, ctorData.options || {});
    this.initOptions();
    
    this.methodCache = {
      activate : this.activate.bindAsEventListener(this)
    }
    
    this.properties = {
      io : {
        loadCB : this.loadPropertiesCallback.bind(this),
        saveCB : this.savePropertiesCallback.bind(this)
      },
      hash : {
        // empty for starters
        labelText : ctorData.label || 'Placeholder Label'
      },
      observers : [
        {elm:'labelText',     evt:'keyup',  fn:this.setLabelText.bindAsEventListener(this, 'extraArg')},
        {elm:'labelPosition', evt:'change', fn:this.setLabelPosition.bindAsEventListener(this)},
        {elm:'labelJustify',  evt:'change', fn:this.setLabelJustification.bindAsEventListener(this)},
        {elm:'requiredYesNo', evt:'change', fn:this.setRequiredYesNo.bindAsEventListener(this)},
        //{elm:'requiredMsg', evt:'keyup',  fn:this.setRequiredMsg.bind(this)}, (?)
        {elm:'hintYesNo',     evt:'change', fn:this.setHintYesNo.bindAsEventListener(this)},
        {elm:'hintText',      evt:'keyup',  fn:this.setHintText.bindAsEventListener(this)},
        {elm:'hintPosition',  evt:'change', fn:this.setHintPosition.bindAsEventListener(this)}
      ]
    };
    this.initProperties();
    
    this.paint();
    this.initObservers(this);
  },
  
  /********************************************************************************************
   * initialize() Helper(s)
   *******************************************************************************************/
  initDOMElement : function(_q) {
    if(zip.debug) console.log("@[Question].initDOMElement() _q = %o", _q);
    
    var DNA = $('DNA_'+_q.type);
    return $(Builder.node(DNA.tagName,{
      id        : _q.questionID,
      style     : 'display:none;',
      className : 'surveyQ '+_q.type
    })).update(DNA.innerHTML || DNA.innerText);
  },

  initOptions : function() {
    if (this.options.questionID) this.setQuestionID(this.options.questionID);
    if (this.options.tempID) this.setTempID(this.options.tempID);
    if (this.options.mfieldID) this.setMfieldID(this.options.mfieldID);
    //if (this.options.label) this.setLabel(this.options.label);// new Zipzing.QuestionLabel(elm_id, {text:this.options.label});
  },
  
  initProperties : function() {
    with (this.properties.hash) {
      if (labelText) this.setLabelText(labelText);
    }
  },
  
  initObservers : function(_this) {
    [this.element, this.element.down('input')].each(function(activator) {
      activator.observe('mousedown', _this.methodCache.activate); // activators *activate* ;)
    });
  },
  
  /********************************************************************************************
   * IO (ajax) callbacks n such
   *******************************************************************************************/
  // EXPERIMENTAL
  //ctorParser : function() { /*parse this.constructorData;*/},
  
  load : function() {},
  
  loadProperties : function() {
    this.Survey.io(this, 'get', this.properties.io.loadURL, this.properties.io.loadCB);
  },
    
  loadPropertiesCallback : function() {  
    // 1) use properties hash to populate properties form:
    $H(this.properties.hash).each(function(p) {
      $(p[0].toString()).value = p[1]; // <--- magic happening
      // DOH! doesn't work for checkboxes... yet
    });
    // 2) attach properties form event observers:
    this.properties.observers.each(function(obs) { $(obs.elm).observe(obs.evt, obs.fn); });
  },
  
  //TODO:SAVING -- next sprint!
  save : function() {},
  saveProperties : function() {},
  savePropertiesCallback : function() {},
  
  _getValue : function(args) {
    return Try.these(
      function() { return args[0].element().checked; },
      function() { return args[0].element().value; },
      function() { return args[0]; }
    )||'stub label';
  },

  /********************************************************************************************
   * Setters
   *******************************************************************************************/
  setQuestionID : function(id) {
    this.options.questionID = id;
  },
  
  setTempID : function(id) {
    this.options.tempID = id;
  },
  
  setMfieldID: function(id) {
    this.options.mfieldID = id;
  },
  
  setLabelText : function() {
    this.element.down('label').update(
      this.properties.hash.labelText = this._getValue(arguments)
    );
  },
  
  setLabelPosition : function() {
    this.element.down('div.questionInput').setStyle({
      display: this.properties.hash.labelPosition = this._getValue(arguments)
    });
  },
  
  setLabelJustification : function() {
    this.element.down('label').setStyle({
      'float': this.properties.hash.labelJustification = this._getValue(arguments)
    });
  },
  
  setRequiredYesNo : function(e) {
    var chk = e.element(), text = chk.up('tr').next('tr');
    this.element.down('span.required').setStyle({
      display: (this.properties.hash.requiredYesNo = chk.checked) ? 'inline' : 'none'
    });
    this.properties.requiredYesNo ? text.appear() : text.fade();
  },
  
  setHintYesNo : function() {
    var chk = $('hintYesNo');
    this.element.down('span.hint').setStyle({
      display: (this.properties.hash.hintYesNo = /*this._getValue(arguments)*/chk.checked) ? 'inline' : 'none'
    });
    //this.properties.hash.hintYesNo ? hint.appear() : hint.fade();
    chk.up('tr').nextSiblings().each(function(prop) {
      if (prop.hasClassName('hint')) chk.checked ? prop.appear() : prop.fade();
    });
  },
  
  setHintText : function() {
    this.element.down('span.hint').update(
      this.properties.hash.hintText = this._getValue(arguments)
    );
  },
  
  setHintPosition : function() {
    this.element.down('span.hint').setStyle({
      display: this.properties.hash.hintPosition = this._getValue(arguments)
    });
  },
  
  /********************************************************************************************
   * GUI Utilities & FX!
   *******************************************************************************************/
  paint : function() {
    this.Row.element.insert(this.element);
    this.Row.autosize(this.element.getHeight());
    this.element.appear(zip.fx.lib.transitions);
  },
  
  erase : function() {
    this.element.puff(zip.fx.lib.transitions);
  },
  
  warn : function() {
    this.element.pulsate(zip.fx.lib.errPulse);
  },
  
  activate : function(e) {
    e.stop(); this.focus();
  },
  
  focus : function() {
    this.loadProperties();
  },
  
  lock : function() {
    this.element.down('input').disable();
  },
  
  unlock : function() {
    this.element.down('input').enable();
  }
});
var Zipzing = Zipzing || {};
//
Zipzing.Row = Class.create({
  /********************************************************************************************
   * Constructor
   *******************************************************************************************/
  initialize : function(ctorData) {
    this.Type = "Zipzing.Row";
    
    this.Panel = ctorData.panel;
    this.Survey = ctorData.survey;    
    this.element = ctorData.element || this.initDOMElement(ctorData, $('DNA_Row'));
    
    this.properties = {
      io : {
        loadURL : this.Panel.properties.io.loadURL,
        loadCB  : this.Panel.properties.io.loadCB
      }
    };
    
    // Row Behaviors
    this.fx = Object.extend({
      sortable:true, only:['surveyQ','surveyBtn','surveyContent'],
        constraint  : 'horizontal',
        containment : $$('.surveyRow'), // all stage rows (any panel)
      droppable:true, accept:'widget'
    }, ctorData.fx || {});
    this.paint();
    zip.fx(this);
    
    // Row Observers
    //this.observers = Object.extend({/*default(s)*/}, ctorData.observers || {/*empty*/});
    this.initObservers();
    
    // Row Buttons
    this._Buttons = ctorData.buttons || [/*empty array*/];
    this.initButtons(this);
    
    // Row Content
    this._Content = ctorData.content || [];
    this.initContent(this);
    
    // Row Questions
    this._Questions = ctorData.questions || [];
    this.initQuestions(this);
    
    this.focus();
  },
  
  /********************************************************************************************
   * initialize() Helpers
   *******************************************************************************************/
  initDOMElement : function(row, DNA) {
    return $(Builder.node(DNA.tagName,{
      id        : 'row_'+row.panel.options.panelID+'-'+row.rowID,
      className : 'surveyRow'
    }));//.update(DNA.innerHTML || DNA.innerText); //none:Row is just empty <div>
  },

  initObservers : function() {
    this.observeDrag();
    this.element.observe('click', this.focus.bind(this) );
    this.element.observe('hover', function(e) {e.stop();}); // hover dead zone
  },
  
  initButtons : function(_this) {
    this.Buttons = [];
    this._Buttons.each(function( _btn ) {
      _this.Buttons.push(new Zipzing.Button({
        survey : _this.Survey,
        row    : _this,
        button : _btn
      }));
    });
    delete this._Buttons;
  },
  
  initContent : function(_this) {
    this.Content = [];
    this._Content.each(function( _ctnt ) {
      _this.Content.push(new Zipzing.Content({
        survey  : _this.Survey,
        row     : _this,
        content : _ctnt
      }));
    });
    delete this._Content;
  },
  
  initQuestions : function(_this) {
    var _Q;
    this.Questions = [];
    this._Questions.each(function( _qtn ) {
      var ctorData = {
        survey   : _this.Survey,
        row      : _this,
        question : _qtn
      };
      switch (q.type) {
        
        // Close-Ended
        case 'Checkbox':
          _Q = new Zipzing.CheckboxQuestion(ctorData);
          break;
        case 'Radio':
          _Q = new Zipzing.RadioQuestion(ctorData);
          break;
        case 'DropDown':
          _Q = new Zipzing.DropDownQuestion(ctorData);
          break;
        
        // Open-Ended + Single-Line
        case 'DateQuestion':
          _Q = new Zipzing.DateQuestion(ctorData);
          break;
        case 'EmailQuestion':
          _Q = new Zipzing.EmailQuestion(ctorData);
          break;
        
        // Open-Ended + Multi-Line
        
        // Matrix
        
        //etc
        
        default:
      }
      _this.Questions.push( _Q );
    });
    delete this._Questions;
  },
  
  /********************************************************************************************
   * zip.fx library callbacks:
   *******************************************************************************************/
  sortUpdateCallback : function(sort) {
    if(zip.debug) console.log('@ Row.sortUpdateCallback( %s ) : this=%o', sort.id, this);
    // do things like:
    //
    // 1) update the questions[] or buttons[] arrays for the row (this.options.questions, this.options.buttons)
    //
    // 2) resize the row, accordingly???
    //      var rowHeight = +/-this.options.rowHeight;
    //      this.element.morph('height:rowHeight'); //?
    //
    // 3) visual feedback... this.element.highlight
  },
  
  dropCallback : function(drag, drop, evt) {
    if (drag.hasClassName('widget')) {
      if (drag.hasClassName('toolboxQ') || drag.hasClassName('toolboxCustom')) this.addQ(drag, drop, evt, this);
      if (drag.hasClassName('toolboxBtn')) this.addBtn(drag, drop, evt, this);
      if (drag.hasClassName('toolboxContent')) this.addContent(drag, drop, evt, this);
    }
  },
  
  /********************************************************************************************
   * *special* Row(s) observer & callback:
   *******************************************************************************************/
  observeDrag : function() {
    // NOTE: Drag observers are very very quirky. They don't behave like I expected!
    Draggables.addObserver({
      element : this.element, // seemingly has NO affect (where, you might expect it to limit to just this element... NOPE!)
      onEnd   : this.onDragEndCallback.bind(this)
    });
  },
  
  onDragEndCallback : function(eventName, drag, evt) {
    if ( this.element.id==drag.element.id && this.element.hasClassName('surveyRow') ) {
      //if(zip.debug) console.log('@ Row.onDragEndCallback( %s, %o, %o ) : this=%o', eventName, drag, evt, this);
      //if(zip.debug) console.log(" --[ASSERT]--> (%s == %s) eval'd: %s",drag.element.id,this.element.id, drag.element.id == this.element.id);
      //if(zip.debug) console.log(" --[ASSERT]--> %s.Panel.element.id (pre-re-assignment): %s",this.element.id,this.Panel.element.id);
      this.Survey.Registry._DraggedRow = this;
      //if(zip.debug) console.log(" --[ASSERT]--> [._DraggedRow].Panel.element.id (pre-re-assignment): %s", this.Survey.Registry._DraggedRow.Panel.element.id);
    }
  },
  
  /********************************************************************************************
   * add/remove/etc:
   *******************************************************************************************/
  addQ : function(widget, drop, evt, _this) {
    var Q, _q = this.questionTemplate(widget);
    switch (widget.id) {
    // DB FIELDS...
      case 'FirstName':
      case 'LastName':
      case 'FullName':
      case 'SalesRepFullName':
      case 'Address':
      case 'Address2':
      case 'HomePhoneExt':
      case 'City':
      case 'Zip'/*Postal*/:
        _q.type = 'SingleLineQ';
        _q.label = widget.title;
        Q = new Zipzing.SingleLineTextQuestion(_q);
        break;
      
      case 'Email':
      case 'SalesRepEmail':
        _q.type = 'EmailQ';
        _q.label = widget.title;
        Q = new Zipzing.EmailQuestion(_q);
        break;
      
      case 'State'/*Province*/:
      case 'Country':
        _q.type = 'DropDownQ';
        _q.label = widget.title;
        Q = new Zipzing.DropDownQuestion(_q);
        break;
        
      case 'HomePhone':
      case 'CellPhone':
      case 'Fax':
        _q.type = 'Telephony';
        _q.label = widget.title;
        //Q = new Zipzing.TelephonyQuestion(_q);
        break;
      
    // CUSTOM FIELDS...
      /*
      case 'Tb_SingleLineQ':
        Q = new Zipzing.SingleLineTextQuestion(_question);
        break;*/
      
      case 'Tb_RadioQ':
        //_question.label = 'Choose one:';
        _q.answers = [
          {label:'1',value:1,score:1},
          {label:'2',value:2,score:2},
          //{label:'option 3',value:3,score:3}
        ];
        //_q.otheranswer = {label:'option <em>other</em>',value:'other',score:0};
        Q = new Zipzing.RadioQuestion(_q);
        break;
      
      case 'Tb_CheckboxQ':
        //_question.label = 'Check any:';
        _q.answers = [
          {label:'1',value:1,score:1},
          {label:'2',value:2,score:2},
          //{label:'option 3',value:3,score:3}
        ];
        //_q.otheranswer = {label:'option <em>other</em>',value:'other',score:0};
        Q = new Zipzing.CheckboxQuestion(_q);
        break;
      
      case 'Tb_DropDownQ':
        _q.label = 'Choose'
        _q.promptext = 'Prompt Text';
        Q = new Zipzing.DropDownQuestion(_q);
        break;
      
      case 'Tb_MatrixQ':
        _q.label = 'Choose one for each category';
        Q = new Zipzing.MatrixQuestion(_q);
        break;
      
      case 'Tb_DateQ':
        _q.label = 'Date';
        Q = new Zipzing.DateQuestion(_q);
        break;
      
      case 'Tb_StateNationQ':
        _q.label = 'State/Province/Nation';
        Q = new Zipzing.DropDownQuestion(_q);
        break
      
      default: Q = {}; break;
    }
    this.Questions.push(Q);
    if(zip.debug) console.log('questions count: '+this.Questions.length);
    //this.registerBehaviors();
  },
  
  questionTemplate : function(widget) {
    var count = this.Questions.length + 1;// TODO: better id generator
    return {
      survey     : this.Survey,
      row        : this,
      key        : count,
      questionID : count, 
      type       : widget.id.split('_')[1] || ''
    };
  },
  
  removeQ : function(question) {
    // remove from array
    // ? panels.pop(panel);
    //--this.options.counters.q;
    
    // remove from DOM
    //question.remove();
    
    // clean up object itself
    
    // GUI effects
    //this.erase(question);
  },
  
  addBtn : function(toolboxBtn, drop, evt, _this) {
    this.Buttons.push(new Zipzing.Button({
      survey : _this.Survey,
      row    : _this,
      button : _this.buttonTemplate()
    }));
    if(zip.debug) console.log('buttons count: '+this.Buttons.length);
    //this.registerBehaviors();
  },
  
  buttonTemplate : function() {
    var count = this.Buttons.length + 1;
    return {
      key      : 'button-' + count,
      buttonID : count
    };
  },
  
  removeBtn : function(button){
    // remove from array
    // ? panels.pop(panel);
    //--this.options.counters.b;
    
    // remove from DOM
    //button.remove();
    
    // clean up object itself
  },

  addContent : function(toolboxContent, drop, evt, _this) {
    this.Content.push(new Zipzing.Content({
      survey  : _this.Survey,
      row     : _this,
      content : _this.contentTemplate()
    }));
    if(zip.debug) console.log('content count: '+this.Content.length);
    //this.registerBehaviors();
  },
  
  contentTemplate : function() {
    var count = this.Content.length + 1;
    return {
      key       : 'content-' + count,
      contentID : count
    };
  },
  
  removeContent : function() {
    //--this.options.counters.c;
  },
  
  /********************************************************************************************
   * GUI Utilities & FX!
   *******************************************************************************************/
  paint : function() {
    this.Panel.element.insert(this.element);
    this.Panel.autosize();
    this.element.appear(zip.fx.lib.transitions);
  },

  erase : function() {
    this.element.puff(zip.fx.lib.transitions);
    this.Panel.autosize(-this.element.getWidth());
  },
  
  focus : function() {
    // BEWARE: this fires when a Question/Button/Content item is clicked as well
    //  (due to the nature of browser event propagation)
    //this.loadProperties();
    if (arguments[0]) arguments[0].stop();
  },
  
  autosize : function(deltaWidth) {
    //if(zip.debug) console.log('Zipzing.Row.autosize( '+deltaWidth+' )');
    // maybe we don't touch the rows... row width will just be sticky to panel width, with overflow:hidden (???)
    //this.element.morph('width:' + (this.element.getWidth() + deltaWidth) +'px', zip.fx.lib.transitions);
    this.Panel.autosize();
  },
  
  lock : function() {
    // TODO: disable sorting
    //       show padlock icon
  },
  
  unlock : function() {
    // TODO: re-enable sorting
    //       hide padlock icon
  },
  
  registerBehaviors : function() {
    this.Panel.Rows.each(function(row) {
      //if(zip.debug) console.log('invoked [Row].registerBehaviors() on '+row.element.id);
      //row.initBehaviors();
    })
  }
});
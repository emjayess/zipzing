Zipzing.debug = true;

var Resizables = {
  set : [],
  observers : [],
  _resizing : false,
  register : function(rszbl) {
    if (this.set.length == 0) {
      //@todo
    }
    this.set.push(rszbl);
  },
  unregister : function(rszbl){
    //@todo
  }
}

Zipzing.Resizable = Class.create({

  initialize : function(ctorData) {
    this.element = ctorData.element || null;

    // Resizable coordinates + boundaries/limits
    //   TODO: Should be a shared (mixin object?) with Panel.class...
    //   in other words... a 'Panel' object should have 'boundaries'
    //   (& not each of the 'Resizable'+'Draggable' sub-objects/behavior-objects)
    this.coords = Object.extend({
      mouseX     : 0, mouseY     : 0,
      lastMouseX : 0, lastMouseY : 0,
      mOffX      : 0, mOffY      : 0,
      elmX       : 0, elmY       : 0,
      elmW       : 0, elmH       : 0,
      diffs      : {
        X:0, Y:0
      },
      bounds     : {
        minW     : 120, minH      : 40,
        minLeft  : 0,   maxLeft   : 9999,
        minTop   : 0,   maxTop    : 9999
      }
    }, ctorData.coords || {});

    // Resizable Grippies
    this.grippies = Object.extend({
      cssBaseClass  : 'resizable',
      activeGrippie : false,
      elemSet       : [],
      tokenSet      : [
        'tl', 'tm', 'tr',
        'ml',       'mr',
        'bl', 'bm', 'br'
      ]
    }, ctorData.grippies || {});
    this.initGrippies(this);

    // Resizable Event Observers Cache
    this.observerCache = {//bind & cache [event] observers
      activate         : this.activate.bindAsEventListener(this),
      deactivate       : this.deactivate.bindAsEventListener(this),
      grippieMouseDown : this.grippieMouseDown.bindAsEventListener(this),
      grippieDrag      : this.grippieDrag.bindAsEventListener(this),
      grippieRelease   : this.grippieRelease.bindAsEventListener(this)
    };
    this.initObservers(this);
  },

  initObservers : function(_this){
    this.element.observe('mouseover', this.observerCache.activate);
    this.element.observe('mouseout',  this.observerCache.deactivate);
    this.grippies.elemSet.each(function(g) {// attach resize grippie observers...
      g.observe('mousedown', _this.observerCache.grippieMouseDown);
    });
  },

  initGrippies : function(_this){
    this.grippies.tokenSet.each(function(token) {// generate grippie DOM elements...
      _this.element.insert( _this.initDOMElement(token) );
    });
    this.grippies.elemSet = this.element.select('.'+this.grippies.cssBaseClass);
  },

  initDOMElement : function(token){
    //generates e.g. <div class="resizable resizable-br">
    var _grippie = Builder.node('div', {
      style     : 'display:none;',
      className : this.grippies.cssBaseClass+' '+this.grippies.cssBaseClass+'-'+token
    });
    _grippie.token = token;
    return _grippie;
  },

  activate : function(e){
    if (!Resizables._resizing) {//prevent [resize] activation of other objects when a resize operation is in-progress!
      if (!this.grippies.activeGrippie) this.focus();
    }
  },

  deactivate : function(e){
    if (!this.grippies.activeGrippie) this.blur();
  },

  grippieMouseDown : function(e){
    this.grippies.activeGrippie = e.element(); /*stop event*/e.stop();
    Resizables._resizing = true;
    this.captureCoordinates();
    this.captureMovement(e);
    document.body.observe('mousemove', this.observerCache.grippieDrag);
    document.body.observe('mouseup', this.observerCache.grippieRelease);
  },

  grippieDrag : function(e){
    if (!this.grippies.activeGrippie) return; /*stop event*/e.stop();
    if (Resizables._resizing) this.resize(e);
  },

  grippieRelease : function(e){
    if (!this.grippies.activeGrippie) return; /*stop event*/e.stop();
    Resizables._resizing = false;
    document.body.stopObserving('mousemove', this.observerCache.grippieDrag);
    document.body.stopObserving('mouseup', this.observerCache.grippieRelease);
    this.blur();
  },

  captureCoordinates : function(){
    this.coords.elmX = this.element.getStyle('left');//parseInt(this.element.style.left);
    this.coords.elmY = this.element.getStyle('top');//parseInt(this.element.style.top);
    this.coords.elmW = parseInt(this.element.getStyle('width'));//offsetWidth;
    this.coords.elmH = parseInt(this.element.getStyle('height'));//offsetHeight;
  },

  captureMovement : function(e){
    this._capturePointer(e);
    this._captureDiffs();
    this._clearMovement();
    this._cachePointer();
  },

  _clearMovement : function(){
    // 'reset' movement offsets
    with (this.coords) { mOffX = mOffY = 0 };
  },

  _capturePointer : function(e){
    this.coords.mouseX = e.pointerX() + document.documentElement.scrollLeft;
    this.coords.mouseY = e.pointerY() + document.documentElement.scrollTop;
  },

  _cachePointer : function(){
    // 'cache' last processed mouse coordinates
    with (this.coords) { lastMouseX = mouseX; lastMouseY = mouseY; }
  },

  _captureDiffs : function(){
    // record the relative mouse movement; Add any previously stored offsets to the calculations
    with (this.coords) {
      diffs.X = mouseX - lastMouseX + mOffX;
      diffs.Y = mouseY - lastMouseY + mOffY;
    }
  },

  _obeyBounds : function(plane){
    //  Bounding Box Notes... bounds checking is the hard bit -- basically for each edge, check
    //  that the element doesn't go under minimum size, and doesn't go beyond its boundary.
    with (this.coords) {
      var _dY = diffs.Y, _dX = diffs.X;
      if (plane == 'v') {// vertical plane
        if (elmH + _dY < bounds.minH) {
          mOffY = (_dY - (diffs.Y = bounds.minH - elmH));
        }/* uncomment for "top" support...
        else if (elmY + elmH + _dY > bounds.maxTop) {
          mOffY = (_dY - (diffs.Y = bounds.maxTop - elmY - elmH));
        }*/
        elmH += diffs.Y;
      }
      if (plane == 'h') {// horizontal plane
        if (elmW + _dX < bounds.minW) {
          mOffX = (_dX - (diffs.X = bounds.minW - elmW));
        }/* uncomment for "left" support...
        else if (elmX + elmW + _dX > this.bounds.maxLeft) {
          mOffX = (_dX - (diffs.X = this.bounds.maxLeft - elmX - elmW));
        }*/
        elmW += diffs.X;
      }
    }
  },

  /*debug*/dumpCoords : function(){
    $H(this.coords).each(function(c) { if(zip.debug) console.log(" ----> this.coords[%s] = %s",c[0],c[1]); });
  },

  focus : function(){
    this.paintGrippies();
    this.captureCoordinates();
  },

  blur : function(){
    this.eraseGrippies();
    this._clearMovement();
    delete this.grippies.activeGrippie;
  },

  paintGrippies : function(){
    this.grippies.elemSet.each(function(g) { g.show(); });
  },

  eraseGrippies : function(){
    this.grippies.elemSet.each( function(g) { g.hide(); });
  },

  resize : function(e){
    this.captureMovement(e);
    if (this.grippies.activeGrippie.token.indexOf('b') >= 0) this._obeyBounds('v'); //BOTTOM (vertical)
    if (this.grippies.activeGrippie.token.indexOf('r') >= 0) this._obeyBounds('h'); //RIGHT (horizontal)
    //@todo: don't use 'with'
    with (this.coords) this.element.setStyle({width:elmW+'px', height:elmH+'px'});
  }
});
/**
 * @Class RadioQuestion
 *  inherits from CloseEndedQuestion, which inherits from Question...
 *
 *  Question
 *    CloseEndedQuestion
 *      RadioQuestion
 */
Zipzing.RadioQuestion = Class.create(
 Zipzing.CloseEndedQuestion, {
  /********************************************************************************************
   * Constructor
   *******************************************************************************************/
  initialize : function($super, ctorData) {
    $super(ctorData);
    
    this.ajax = {
      //view     : '/IMA5/?method=cSurveyProperties.',
      //action   : 'displayRadioProperties',
      propsURL   : '/IMA5/?method=cSurveyProperties.displayRadioProperties',
      callback   : this.register.bind(this, 'properties') // cache n curry
    };
    
    this.InputImage = ctorData.inputimage || this.inputImageTemplate();
    this.initOptions();
  },
  
  /********************************************************************************************
   * initialize() Helpers
   *******************************************************************************************/
  initOptions : function() {
    if (this.InputImage) this.setInputImage(this.InputImage);
  },
  
  inputImageTemplate : function() {
    // TODO: http://ryanfait.com/resources/custom-checkboxes-and-radio-buttons/
    /*
    return "R0lGODlhHgAZAMQAAPFmIf3h1czMzPiUYtzc3PKARP////i9m+fn5/etiPnayPNqJtPT0+7u7v/x"+
           "6vnJsvSBS/FoIOnq7PR5Pf7o3fabb/q1lP/y7PN7P////wAAAAAAAAAAAAAAAAAAAAAAACH5BAEH"+
           "ABkALAAAAAAeABkAAAVhYCaOZGmeaKqubOu+cCzPNIw0NHWdAhEHgwnEYiIIeq8CYAk4lCRHAUPS"+
           "Yi4hpUb0SMCprABMdntErBboSKQyjvpYCXUEQikhjtOX4vBwmBgMZjUkEm+Dh4iJiouMjY4wIQA7";
           */
  },
  
  /********************************************************************************************
   * Ajax callback(s)
   *******************************************************************************************/
  register : function() {
    if(zip.debug) console.log("@%s.register()", this.element.id);
  },
  
  /********************************************************************************************
   * Setters
   *******************************************************************************************/
  setInputImage : function(img) {
    if(zip.debug) console.log("@ RadioQuestion.setInputImage(%o)", img);
    
    //paint:
    this.element.select('input[type="radio"]').each(function(radio) {
      if(zip.debug) console.log('---> select(input[type="radio"]) actually worked... %o',radio);
      radio.setStyle({ backgroundImage:img });
    });
  }
});
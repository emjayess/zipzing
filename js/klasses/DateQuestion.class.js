var Zipzing = Zipzing || {};
/**
 * @Class DateQuestion
 *  inherits from SingleLineText, which inherits from OpenEnded, which inherits from Question...
 *
 *  Question
 *    OpenEndedQuestion
 *      SingleLineTextQuestion
 *        DateQuestion
 */
Zipzing.DateQuestion = Class.create(
 Zipzing.SingleLineTextQuestion, {
  /********************************************************************************************
   * Constructor
   *******************************************************************************************/
  initialize : function($super, ctorData) {
    $super(ctorData);
    
    this.setShowIcon(ctorData.showicon || true);
    
    this.observerCache = {
      register : this.register.bind(this, 'properties') // cache n curry
    };
    
    this.ajax = {
      propsURL : '/IMA5/?method=cSurveyProperties.displayDateProperties',
      callback : this.observerCache.register
    };
    
    this.focus();
  },
  
  initObservers : function() {
    
  },
  
  register : function(what) {
    if(zip.debug) console.log("@%s.register()", this.element.id);
    
    switch (what) {
      case 'properties':
        this.initObservers(this, 'properties');
        //this.initProperties();
        break;
    }
  },
  
  /********************************************************************************************
   * Setter(s)
   *******************************************************************************************/
  setShowIcon : function(show) {
    this.showIcon = show;
    // paint...
  }
});
var Zipzing = Zipzing || {};
/**
 * @class CloseEndedQuestion
 *  class def for a Survey *Close Ended* Question...
 *
 * @superclass Question
 *  inherits from Zipzing.Question
 */
Zipzing.CloseEndedQuestion = Class.create(
 Zipzing.Question, {
  /********************************************************************************************
   * Constructor
   *******************************************************************************************/
  initialize : function($super, ctorData) {
    $super(ctorData);

    // CloseEnded Options
    this.options = Object.extend({
      answersPerRow : 1,
      font          : null //new Zipzing.Font(),
    }, ctorData.options || {});

    // CloseEnded Effects
    this.fx = Object.extend({/*default(s)*/}, ctorData.fx || {/*empty*/});

    // CloseEnded Answers
    this.initAnswers(ctorData.answers || []);
    this.initOtherAnswer(ctorData.otheranswer || null);
  },

  /********************************************************************************************
   * initialize() Helpers
   *******************************************************************************************/
  //initOptions : function() {},
  //initFx : function() {},

  initAnswers : function(answers) {
    this.Answers = [];
    var _this = this;
    answers.each(function(a,idx) {
      //a.question = _this;
      a.element = _this.element.down('div'/*div.answer OR div#answerID*/,idx+1);
      _this.Answers.push(new Zipzing.QuestionAnswer(a));
    });
  },

  initOtherAnswer : function(a) {
    if (a) {
      a.question = this;
      a.element = this.element.down('div'/*div.otheranswer*/,3);
      this.OtherAnswer = new Zipzing.OtherAnswer(a);
    }
  },

  /********************************************************************************************
   * Setters
   *******************************************************************************************/
  setAnswersPerRow : function(num){
    this.options.answersPerRow = num;
  },

  setAnswers : function(){
    ;
  },

  setOtherAnswer : function(other){
    this.OtherAnswer = other;
    // paint
  },

  /********************************************************************************************
   * add/remove/etc
   *******************************************************************************************/
  addAnswer : function() {
    ;
  },

  removeAnswer : function() {
    // remove from array
    // ? answers.pop(answer);

    // remove from DOM
    //this.Answers[].element.remove();

    // clean up object itself
  }
});
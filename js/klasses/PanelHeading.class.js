Zipzing.PanelHeading = Class.create({

  /********************************************************************************************
   * Constructor
   *******************************************************************************************/
  initialize : function(ctorData) {
    this.element = ctorData.element || this.initDOMElement(ctorData.header);
    
    // PanelHeading Options
    this.options = Object.extend({
      shape     : "square",
      margin    : "0px",
      border    : new Zipzing.Border(ctorData),
      bgcolor   : "ffffff",
      isVisible : true,
      font      : new Zipzing.Font(ctorData) 
    }, ctorData.options || {});
    
    // PanelHeading Behaviors
    this.behaviors = Object.extend({/*defaults*/}, ctorData.behaviors || {/*empty*/});
    this.initBehaviors();
  },
  
  
  /********************************************************************************************
   * initialize() Helpers
   *******************************************************************************************/
  initDOMElement : function(h) {
    // Builder.node ...
  },
  
  initOptions : function() {
    if (this.options.shape) this.setShape(this.options.shape);
    if (this.options.margin) this.setMargin(this.options.margin);
    if (this.options.border) this.setBorder(this.options.border);
    if (this.options.bgcolor) this.setBGColor(this.options.bgcolor);
    if (this.options.isVisible) this.setVisibility(this.options.isVisible);
  },
  
  initBehaviors : function() {},
  
  
  /********************************************************************************************
   * Setters
   *******************************************************************************************/
  setShape : function(shape) {
    this.options.shape=shape;
  },
  
  setMargin : function(margin) {
    this.options.margin = margin;
  },
  
  setBorder : function(border) {
    this.options.border = border;
  },
  
  setBGColor : function(bgcolor) {
    this.options.bgcolor = bgcolor;
  },
  
  setVisibility : function(isVisible) {
    this.options.isVisible = isVisible;
  }
});
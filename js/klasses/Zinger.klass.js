var Zipzing = Zipzing || {};
//
Zipzing.Zinger = Class.create({

  initialize : function(ctorData){
    this.element = $(ctorData.element) || null;
    this.ZingerID = ctorData.zingerID;
    // Zinger [Global] Registry
    this.Registry = {_zindex:4000};
    this.initToolbox(this);
    this.initTrash();
    this.options = Object.extend(
      {/*defaults*/},
      ctorData.options || {/*empty*/}
    );
    this.initOptions();

    this.behaviors = Object.extend({
      FxDuration : {duration:0.5}
    }, ctorData.behaviors || {});
    this.initBehaviors();

    this._Pages = ctorData.pages || [];
    this.initPages(this);

    this.observers = Object.extend({/*defaults*/}, ctorData.observers || {/*empty*/});
    this.initObservers(this);

    // GUI ASSIST (?) TODO: clean this up
    //$('related').hide();
    //$('wrapper').style.width='100%';
    //$('hidesidebar').hide();
    //$('restoresidebar').show();
  },

  initDOMElement : function(id){
    return $(id); // TODO!
  },

  initPages : function(_this){
    this.Pages = [];
    this._Pages.each(function(p) {
      _this.Pages.push(new Zipzing.Page({
        zinger : _this,
        page   : p
      }));
    });
    delete this._Pages;
    this.pageNavigation = $('pagesNav');
  },

  initPageThumbnails : function(){
    //TODO: Page Thumbnails in the #pageBar ...
  },

  initToolbox : function(_this){
    $$('.widget').each(function(item) {// NEEDS WORK: do we want toolbox widgets to have 'snap'? Maybe panels only?
      new Draggable(item,{
        snap: item.hasClassName('toolboxPanel') ? [10,10] : false,
        revert: true,//'failure',//false,
        zindex: null,
        reverteffect: function(element, top_offset, left_offset) {
          element._revert = new Effect.Move(element, { x: -left_offset, y: -top_offset, duration: 0});
        },
        ghosting: false,
        //callbacks[onStart|onDrag|onEnd]
        onStart : _this.toolboxDragStartCallback.bind(_this),
        //onDrag  : _this.toolboxDragCallback.bind(_this),
        onEnd   : _this.toolboxDragEndCallback.bind(_this)
      });
    });
  },

  toolboxDragCallback : function(drag, e){
    //@todo...
    if(Zipzing.debug || false){
      console.log(
       "@ [widget].toolboxDragCallback(drag:%o, e:%o), drag.style.left=%s, drag.style.top=%s, drag.elm.offsetParent=%o",
       drag, e,
       drag.element.style.left,
       drag.element.style.top,
       drag.element.getOffsetParent()
      );
    }
  },

  toolboxDragStartCallback : function(drag, e){
    this._toolboxDragging = true;
    this.pageNavigation.hide();
    //@todo...
    // capture mouse-click for compensation later
    //var widgetOffsets = drag.viewportOffset();
    //drag.compensateX = drag.element.style.left
    //drag.compensateY
  },
  toolboxDragEndCallback : function(drag, e){
    this._toolboxDragging = false;
  },

  initTrash : function(){
    this.TrashCan =/*@todo:cleanup*/$('trash') || $('can');
    Droppables.add(this.TrashCan, {
      accept: [
        'zingerRow', 'zingerPanel',
        'zingerQuestion', 'zingerQ',
        'zingerContent', 'zingerButton'
      ],
      onDrop: this.onTrashDropCallback.bind(this)
    });
    this.TrashCan.observe('mouseover', this.trashFocus.bindAsEventListener(this));
    this.TrashCan.observe('mouseout', this.trashBlur.bindAsEventListener(this));
  },

  onTrashDropCallback : function(junk, trash, evt){
    if(Zinger.debug) console.log("@ Zinger.onTrashDropCallback(%o, %o, %o)",junk,trash,evt);
    //@todo
    junk.remove();
  },

  trashFocus : function(){
    // TODO: .morph()  // poof animation!
    this.TrashCan.style.width = '25px';
    $('floatlayer').style.padding = '5px';
  },

  trashBlur : function(){
    //@todo: .morph()
    this.TrashCan.style.width = '16px';
    $('floatlayer').style.padding = '2px';
  },

  initOptions : function(){},

  initBehaviors : function(){},

  initObservers : function(_this){
    //this.pageNavigation.observe('click', this.togglePanels.bindAsEventListener(_pages,'pages','middle'));
    $('pagesNavFxWrap').observe('mouseover', this.pageNavHover.bindAsEventListener(this));
    $$('.pageThumbnail').each(function(thumb, i) {
      thumb.observe('mouseover', _this.pageThumbnailHover.bindAsEventListener(_this));
      //thumb.observe('click', this.Pages[i].slide.bind(this));
    });

    $('toggleCustom').observe('click', this.toggleTools.bindAsEventListener(this, $('toggleCustom')));
    $('toggleElements').observe('click', this.toggleTools.bindAsEventListener(this, $('toggleElements')));

    $('canvasExpand').observe('click', this.expand.bind(this));
    $('canvasCollapse').observe('click', this.collapse.bind(this));
    $('canvasExpandLeft').observe('click', this.expandLeft.bind(this));
    $('canvasCollapseLeft').observe('click', this.collapseLeft.bind(this));
    $('canvasExpandRight').observe('click', this.expandRight.bind(this));
    $('canvasCollapseRight').observe('click', this.collapseRight.bind(this));

    $('link_properties').observe('click', this.swapProperties.bindAsEventListener(this, 'properties'));
    $('link_css').observe('click', this.swapProperties.bindAsEventListener(this, 'css'));
  },

  setZingerID : function(id){
    this.options.id = id;
  },

  setPages : function(pages){
    this.options.pages = pages;
  },

  setActor : function(actor){
    this.Actor = actor;
  },

  addPage : function(_this) {
    this.Pages.push(new Zipzing.Page({
      zinger : _this,
      page   : _this.pageTemplate()
    }));
  },

  pageTemplate : function(){
    var pageCount = this.Pages.length + 1;
    return {
      key       : 'page-' + pageCount,
      pageID    : pageCount,
      options   : {},
      behaviors : {},
      observers : {}
    };
  },

  removePage : function(){
    //@todo;
  },

  reorderPages : function() {
    //@todo;
  },

  expand : function(e){
    $('canvasExpand').hide();
    $('canvasCollapse').show();
    this.expandLeft(e);
    this.expandRight(e);
  },

  collapse : function(e){
    $('canvasCollapse').hide();
    $('canvasExpand').show();
    this.collapseLeft(e);
    this.collapseRight(e);
  },

  //@todo: cache DOM selections for all the expand/collapse elements below...

  expandLeft : function(e) {// fomerly hidePageItems()
    $('canvasExpandLeft').hide();
    $('canvasCollapseLeft').show();
    $('tools').hide();
  },

  collapseLeft : function(e) {
    $('canvasCollapseLeft').hide();
    $('canvasExpandLeft').show();
    $('tools').show();
  },

  expandRight : function(e) {
    $('canvasExpandRight').hide();
    $('canvasCollapseRight').show();
    $('props').hide();
  },

  collapseRight : function(e) {
    $('canvasCollapseRight').hide();
    $('canvasExpandRight').show();
    $('props').show();
  },

  fullscreen : function() {
    //@todo
  },

  swapProperties : function(link) {
    if(Zipzing.debug) console.log("@Zinger.swapProperties(%s)", link);
    /*@todo
      function swapPropertiesCSS(panel) {
          if (panel == 'properties') {
            $('propertiesname').innerHTML = "Question Type";
            $('link_properties').style.color = "##193C76";
            $('link_css').style.color = "##999999";
            $('cssproperties').hide();
            $('itemproperties').show();
          }
          else if (panel == 'css') {
            $('propertiesname').innerHTML = "CSS Editor";
            $('link_css').style.color = "##193C76";
            $('link_properties').style.color = "##999999";
            $('itemproperties').hide();
            $('cssproperties').show();
          }
      }
      */
  },

  toggleTools : function(e, toggle){
    //if(engage.debug) console.log("@[Zinger].toggle(e:%o), this=%o", e, this);
    var fx = this.behaviors.FxDuration;

    if (toggle.hasClassName('collapse')) {
      if (toggle.id == "toggleCustom") $('customquestions').blindUp(fx);
      if (toggle.id == "toggleElements") $('pageelements').blindUp(fx);
      toggle.removeClassName('collapse').addClassName('expand');
    }
    else if (toggle.hasClassName('expand')) {
      if (toggle.id == "toggleCustom") $('customquestions').blindDown(fx);
      if (toggle.id == "toggleElements") $('pageelements').blindDown(fx);
      toggle.removeClassName('expand').addClassName('collapse');
    }
      /*
      function toggleItems(divtag) {
          $(divtag).toggle();
          var toggleheight = $(divtag).style.height.replace('px','');

          var toggleImage = "hideshow_" + divtag;
          var currentheight = $('defaultquestions').style.height.replace('px','');

          if ($(divtag).style.display == '') {
            //hidden, show plus sign
            $(toggleImage).src = '#session.imarkconfig.variables.imrURL#img/zinger/resize_options_minimize.gif';
            var newheight =  (currentheight*1) - (toggleheight*1) + 'px';
            $('defaultquestions').style.height = newheight;
          }
          else {
            //displayed, show minus sign
            $(toggleImage).src = '#session.imarkconfig.variables.imrURL#img/zinger/resize_options_maximize.gif';
            var newheight = (toggleheight*1) + (currentheight*1) + 'px';
            $('defaultquestions').style.height = newheight;
          }
      }*/
  },

  togglePanels : function(e, panel, gotosize){
    if(Zipzing.debug){
      console.log("@ %s.togglePanels(%o)", this.id, arguments);
      console.log(' ---> @here? panel=%s; gotosize=%s',panel,gotosize);
    }

    //@todo: cache DOM element selections to optimize performance of this...

          if(panel=='stage'){
            if(gotosize=='maximize'){
              //stage full, pages minimize, properties minimize
              $('pagesNavThumbnails').hide();
              $('pagesNavThumbnails').style.height = "";
              $('stage_1').style.height = "463px";
              $('stage_1').show();
              $('size_pagename').src = '';//@todo //"/zipzing/images/zinger/resize_options_maximize_disabled.gif";
            }
            else if(gotosize=='middle'){
              //stage middle, pages minimize, properties middle
              $('pagesNavThumbnails').hide();
              $('pagesNavThumbnails').style.height = "";
              $('stage_1').style.height = "342px";
              $('stage_1').show();
              $('size_pagename').src = '';//@todo //"/zipzing/images/zinger/resize_options_maximize.gif";
            }
          }
          else if(panel=='pages'){
            var panelsize = $('pagethumbnails').style.height;

            if (gotosize=='maximize' && (panelsize=='' || panelsize=='115px')){
              //stage minimize, pages full, properties minimize
              $('stage_1').hide();
              $('pagesNavThumbnails').style.height = "457px";
              $('pagesNavThumbnails').show();
            }
            else if(gotosize=='middle' && (panelsize == '' || panelsize == '457px')){
              $('pagesNavThumbnails').style.height = "115px";
              $('pagesNavThumbnails').show();
              $('Page1').style.height = "374px";
              $('Page1').show();
            }
            else{
              //stage full, pages minimize, properties minimize
              $('pagesNavThumbnails').hide();
              $('pagesNavThumbnails').style.height = "";
              $('Page1').style.height = "495px";
              $('Page1').show();
            }
          }
  },

  pageNavHoverCancel : function(){
    return this._toolboxDragging; // || this._otherStopCondition || this._yetAnotherStopCondition;
  },

  pageThumbnailHover : function(e){
    if (this.pageNavHoverCancel()) return;
    // continue:
    //this._pageNavHoverState = true;
    $('pagesNavThumbnails').show();
  },

  pageNavHover : function(e){
    if (this.pageNavHoverCancel()) return;
    //if (this._pageNavHoverState) return;
    if(engage.debug) console.log("@ %s.pageNavHover(%o)", this.element.id, arguments);
    //var _pane = $('pages');
    //e.type == 'mouseover' ? _pane.appear(this.behaviors.FxDuration) : _pane.fade(this.behaviors.FxDuration);
    //if(e.type == 'mouseover') this.togglePanels(e,'pages','middle');

    var _blurFxDuration = {duration:1.5};
    var _focusFxDuration = {duration:0.3};

        if(arguments[1]=='mouseout'){
        //if (e.type == 'mouseout') {
          //$('Page1').morph('height:400px;', _blurFxDuration);
          //$('pagesNavFxWrap').morph('height:0;', _blurFxDuration);
          if(engage.debug) console.log("event target: %s", Event.element(e).id);
          $('pagesNavThumbnails').fade(_blurFxDuration);
          $('Page1').stopObserving('mouseover');
        }
        else{
          //$('pagesNavFxWrap').morph('height:95px;', _focusFxDuration);
          //$('Page1').morph('height:305px;', _focusFxDuration);
          $('pagesNavThumbnails').appear(_focusFxDuration);
          this._pageNavHoverState = true;
          // register observer
          $('Page1').observe('mouseover', this.pageNavHover.bindAsEventListener(this, 'mouseout'));
        }
  },

  loading : function(){
    //@todo: display a "loading..." visual indicator while Zinger Builder UI is loading
  },

  io : function(obj, httpMethod, url, cb){
    if (Zipzing.debug) console.log("@[Zinger].io(%o)", arguments);

    this.Registry.__Actor = this.Registry._Actor;
    this.Registry._Actor = obj;

    var props = $('itemproperties');
    if(this.Registry._Actor != this.Registry.__Actor){
      props.setStyle({opacity:0.3});
      new Ajax.Updater(props.up('div'), url, {
        method     : httpMethod,
        onComplete : function(){
          cb();
          props.setStyle({opacity:1});
        }
      });
    }
  },

  loadProperties : function(url,cb){
    //@todo: "loading" indicator in the properties sidebar...
    if (Zipzing.debug) console.log("@[Zinger].loadProperties(%o)", arguments);

    var props = $('itemproperties');
    if(this.Registry._Actor != this.Registry.__Actor){
      // loading visuals
      props.setStyle({opacity:0.3});
      //Element.clonePosition($('props').down('.loading'), props);
      //$('props').show();
      new Ajax.Updater(props.up('div'), url, {
        method     : 'get',
        onComplete : function(){
          //if (Zipzing.debug) console.log("io callback: %s", cb);
          cb();//callback
          props.setStyle({opacity:1});
          //$('props').down('.loading').hide();
        }
      });
    }
  }
});
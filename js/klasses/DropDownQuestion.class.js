var Zipzing = Zipzing || {};
/**
 * @Class DropDownQuestion
 *  inherits from CloseEnded, which inherits from Question...
 *
 *  Question
 *    CloseEndedQuestion
 *      DropDownQuestion
 */
Zipzing.DropDownQuestion = Class.create(
 Zipzing.CloseEndedQuestion, {
  /********************************************************************************************
   * Constructor
   *******************************************************************************************/
  initialize : function($super, ctorData) {
    $super(ctorData);
    
    if(zip.debug) console.log(this.element);
    
    this.promptText = ctorData.promptext || "";
    this.initOptions();
    
    this.ajax = {
      //view     : '/IMA5/?method=cSurveyProperties.',
      //action   : 'displayDropDownProperties',
      propsURL   : '/IMA5/?method=cSurveyProperties.displayDropdownProperties',
      callback   : this.register.bind(this, 'properties') // cache n curry
    };
    
    this.lock();
    this.focus();
  },
  
  /********************************************************************************************
   * initialize() Helpers
   *******************************************************************************************/
  initOptions : function($super) {
    $super();
    if (this.promptText) this.setPromptText(this.promptText);
  },
  
  /********************************************************************************************
   * Ajax callback(s)
   *******************************************************************************************/
  register : function() {
    if(zip.debug) console.log("@%s.register()", this.element.id);
  },
  
  /********************************************************************************************
   * Setters
   *******************************************************************************************/
  setPromptText : function(text) {//@@todo:'onchange' observer in properties panel
    this.promptText = text;
    // paint
    this.element.down('option',0).update(text);
  },
  
  /********************************************************************************************
   * add/remove/etc
   *******************************************************************************************/
  addPromptText : function(text) {
    this.prompText = text;
    //
    // 'push' a new <option> element to the top of the <select> element
    this.element.down('select').insert(
      new Builder.node('option',{
        id        : this.element.id + '_promptText',
        className : 'promptText'
      }).update(text)
    );
  },
  
  removePromptText : function() {
    this.promptText = "";
    //
    //pop the 'promptext' <option> element from the <select> element
    this.element.down('option.promptText').remove();
  },
  
  /********************************************************************************************
   * GUI Utilities & FX!
   *******************************************************************************************/
  lock : function() {
    this.element.down('select').disable();
  },
  
  unlock : function() {
    this.element.down('select').enable();
  }
});

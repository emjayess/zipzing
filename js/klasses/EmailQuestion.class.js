/**
 * @Class EmailQuestion
 *  inherits from SingleLineText, which inherits from OpenEnded, which inherits from Question...
 *  > Question
 *  >   OpenEndedQuestion
 *  >     SingleLineTextQuestion
 *  >       EmailQuestion
 */
Zipzing.EmailQuestion = Class.create(
 Zipzing.SingleLineTextQuestion, {
  /********************************************************************************************
   * Constructor
   *******************************************************************************************/
  initialize : function($super, ctorData) {
    $super(ctorData);
    
    //this.setShowConfirmation(ctorData.showconfirmation || false);
    //this.setConfirmationLabel(ctorData.confirmationlabel || "");
    
    this.properties = {
      io : Object.extend({
        loadURL :/*HTTP GET*/  '^load/action', // @todo backend
        saveURL :/*HTTP POST*/ '^save/action'  // @todo backend
      }, this.properties.io),
      hash : Object.extend({/*none*/}, this.properties.hash),
      observers : this.properties.observers.concat([
        {elm:'emailMaxChars',    evt:'change', fn:this.setMaxChars.bindAsEventListener(this)},
        {elm:'emailInputPixels', evt:'change', fn:this.setInputPixels.bindAsEventListener(this)}
      ])
    };
    
    this.focus();
  },
  
  /********************************************************************************************
   * Initialize helper(s)
   *******************************************************************************************/
  
  /********************************************************************************************
   * IO (ajax) callback(s) n such
   *******************************************************************************************/
  // EXPERIMENTAL...
  // parseProperties : function() { var observers = $('PropertyPane').select('SPECIALoBSERVERcLASS'); },
  
  /********************************************************************************************
   * Setter(s)
   *******************************************************************************************/
  setMaxChars : function(e) {
    if(zip.debug) console.log("@Email.setMaxChars(%o)", arguments);
    /* old
    var elm = e.element();
    this.element.down('input').maxLength = this.properties.hash.maxChars = elm.value;
    */
  },
  
  setInputPixels : function() {
    if(zip.debug) console.log("@Email.setInputPixels(%o)", arguments);
  }
});
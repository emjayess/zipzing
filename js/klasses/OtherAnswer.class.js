Zipzing.OtherAnswer = Class.create(
 Zipzing.QuestionAnswer, {// this should inherit from 'Zipzing.QuestionAnswer' ???
  
  /********************************************************************************************
   * Constructor
   *******************************************************************************************/
  initialize : function($super, ctorData) {
    $super(ctorData);
    this.setRatio(ctorData.ratio || /*default*/"50/50");
  },
  
    
  /********************************************************************************************
   * Setters:
   *******************************************************************************************/ 
  setRatio: function(ratio) {
    this.Ratio = ratio;
  }
});
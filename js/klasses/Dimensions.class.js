Zipzing.Dimensions = Class.create({
  
  /********************************************************************************************
   * Constructor
   *******************************************************************************************/
  initialize : function(ctorData) {
    this.Survey = ctorData.survey;
    this.Panel  = ctorData.panel;
    this.Row    = ctorData.row;
    //this.element = ctorData.element || this.initDOMElement(ctorData.dimension);
    
    // Dimensions Options
    this.options = Object.extend({
      width  : null,
      height : null
    }, ctorData.options || {});
    this.initOptions();
    
    // Dimensions Behaviors
    this.behaviors = Object.extend({/*defaults*/}, ctorData.behaviors || {/*empty*/});
    this.initBehaviors();
  },
  
  
  /********************************************************************************************
   * initialize() Helpers
   *******************************************************************************************/
  initDOMElement : function(d) {
    // Builder.node ... 
  },
  
  initOptions : function() {
    if(this.options.width) this.setWidth(this.options.width);
    if(this.options.height) this.setHeight(this.options.height);
  },
  
  initBehaviors : function() {},
  
  
  /********************************************************************************************
   * Setters:
   *******************************************************************************************/ 
  setWidth : function(width) {
    this.options.width = width;
  },
  
  setHeight : function(height) {
    this.options.height = height;
    //
  }
});
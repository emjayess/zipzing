Zipzing.Border = Class.create({
  
  initialize : function(elm_id){
    var defaults = {
      weight: "1px",
      color: "000000"   
    };
    this.element = $(elm_id);
    this.options = Object.extend(defaults, arguments[1] || {});
    if (this.options.weight) this.setWeight(this.options.weight);
    if (this.options.color) this.setColor(this.options.color);
  },

  setWeight: function(wt){
    this.options.weight = wt;
    // reflect to stage
    //this.element.style.borderWeight = wt;
  },
  
  setColor: function(color){
    this.options.color = color;
    // reflect to stage
    //this.element.style.borderColor = color;
  } 
});
var Zipzing = Zipzing || {};
//
Zipzing.MultiLineTextQuestion = Class.create(
  Zipzing.OpenEndedQuestion, {
  /********************************************************************************************
   * Constructor
   *******************************************************************************************/
  initialize : function($super, ctorData) {
    $super(ctorData);
    
    this.ajax = {
      //view   : '^view/url',
      //action : '^view.action',
      propsURL : '^properties/url', //@todo: [default] properties fixture
      callback : this.register.bind(this, 'properties') // cache n curry
    };
    
    this.options = Object.extend({
      width : "200px",
      rows  : "4"
    }, ctorData.options || {});
    this.initOptions();
    
    this.behaviors = Object.extend({
      charCountWidget : false
    }, ctorData.behaviors || {});
    this.initBehaviors();
    
    //@todo: I think there's gonna be a bug here, 
    //      > Question.paint() needs to fire before any behaviors are applied
    this.focus();
  },
  
  /********************************************************************************************
   * initialize() Helpers
   *******************************************************************************************/
  initOptions : function($super) {
    $super();
    //
    if (this.options.width) this.setWidth(this.options.width);
    if (this.options.rows) this.setRows(this.options.rows);
  },
  
  initBehaviors : function($super) {
    $super();
    //
    if (this.behaviors.charCountWidget) this.setCharCountWidget(this.behaviors.charCountWidget);
  },
  
  /********************************************************************************************
   * Ajax callback(s)
   *******************************************************************************************/
  register : function() {
    if(zip.debug) console.log("@%s.register()", this.element.id);
  },
  
  /********************************************************************************************
   * Setters
   *******************************************************************************************/
  setWidth : function(width) {
    this.options.width = width;
  },
  
  setRows : function(rows) {
    this.options.rows = rows;
  },
  
  setCharCountWidget : function(show) {
    this.behaviors.charCountWidget = show;
  }
});
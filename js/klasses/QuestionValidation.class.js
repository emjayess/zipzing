Zipzing.QuestionValidation = Class.create({
  
  /********************************************************************************************
   * Constructor
   *******************************************************************************************/
  initialize : function(ctorData) {
    //this.element = ctorData.element || this.initDOMElement(ctorData.validation);
    
    // Validation Options
    this.options = Object.extend({
      errorMessage: "",
      requiredMessage: "",
      minimum: 0,
      maximum: 0,
      isRequired: false,
      dataType: "string"
    }, ctorData.options || {});
    this.initOptions();
    
    //this.behaviors = Object.extend({/*defaults*/}, ctorData.behaviors || {/*empty*/});
  },
  
  
  /********************************************************************************************
   * initialize() Helpers
   *******************************************************************************************/
  initDOMElement : function(v) {
    // Builder.node ...
  },
  
  initOptions : function() {
    if (this.options.errorMessage) this.setErrorMessage(this.options.errorMessage);
    if (this.options.requiredMessage) this.setRequiredMessage(this.options.requiredMessage);
    if (this.options.minimum) this.setMinimum(this.options.minimum);
    if (this.options.maximum) this.setMaximum(this.options.maximum);
    if (this.options.isRequired) this.setIsRequired(this.options.isRequired);
    if (this.options.dataType) this.setDataType(this.options.dataType);
  },
  
  
  /********************************************************************************************
   * Setters
   *******************************************************************************************/
  setErrorMessage : function(msg) {
    this.options.errorMessage = msg;
  },
  
  setRequiredMessage : function(msg) {
    this.options.requiredMessage = msg;
  },
  
  setMinimum : function(minimum) {
    this.options.minimum = minimum;
  },
  
  setMaximum : function(maximum) {
    this.options.maximum = maximum;
  },
  
  setIsRequired : function(isRequired) {
    this.options.isRequired = isRequired;
  },
  
  setDataType : function(dataType) {
    this.options.dataType = dataType;
  }
});
Zipzing.Page = Class.create({
  
  /********************************************************************************************
   * Constructor
   *******************************************************************************************/
  initialize : function(ctorData) {
    this.Key = ctorData.page.key;
    
    this.Survey = ctorData.survey;
    this.element = $('Page'+ctorData.page.pageID) || this.initDOMElement(ctorData.page);
    
    // Page Options
    this.options = Object.extend({
      name : ""
    }, ctorData.page.options || {/*empty*/});
    this.initOptions();
    
    // Page Behaviors
    this.behaviors = Object.extend({
      droppable : true  
    }, ctorData.page.behaviors || {/*empty*/});
    
    //this.paint();
    this.initBehaviors();
    
    // Page Observers
    this.initObservers(this);
    
    // Page Offsets n Coordinates
    this.coords = {
      CssP : this.element.positionedOffset() // CSS-P[ositioning] offsets
    }
    
    // Page Panels
    this._Panels = ctorData.page.panels || [];
    this.initPanels(this);
  },
  
  
  /********************************************************************************************
   * initialize() Helpers
   *******************************************************************************************/
  initDOMElement : function(page) {
    return $('Page'+page.pageID); // TODO!
    /*
    var DNA = $('DNA_Page');
    return Builder.node(DNA.tagName,{
      id        : page.pageID,
      className : 'surveyPage'
    }).update(DNA.innerHTML);
    */
  },
  
  initOptions : function() {
    //if (this.options.pageID) this.setPageID(this.options.pageID);
    if (this.options.name) this.setName(this.options.name); 
    //if (this.options.panels) this.setPanels(this.options.panels);
  },
  
  initBehaviors : function() {
    if (this.behaviors.droppable) this.makeDroppable();
  },
  
  initObservers : function(_this) {
    //$('pageSlider').observe('click', this.slide.bindAsEventListener(this));
  },

  initPanels : function(_this) {
    this.Panels = [];
    this._Panels.each(function( _panel ) {
      _this.Panels.push(new Zipzing.Panel({
        survey : _this.Survey,
        page   : _this,
        panel  : _panel
      }));
    });
    delete this._Panels;
  },
  
  template : function() {
    return {
      key : 'page-1-111', // dynamic
      pageID : 'Page1',   // dynamic
      options : { name:"" },
      behaviors : { droppable:true },
      panels : [
        { key : 'panel-1-1-111', // dynamic
          panelID : '1',         // dynamic
          options : {
            panelID : 'panel-tmpID-1' // dynamic
          },
          behaviors : {
            sortable : true,
            droppable : true
          }
        }
      ]
    };
  },
  
  
  /********************************************************************************************
   * Behaviors:
   *******************************************************************************************/
  makeDroppable : function() {
    Droppables.add(this.element.id, {
      accept: ['surveyPanel', 'toolboxPanel'],
      //callbacks=[onHover|onDrop]
      onDrop: this.dropCallback.bind(this), // binds 'Page' object to the 'this' keyword in the scope of 'dropcallback' (instead of the Draggable)!!!
      onHover: this.hoverCallback.bind(this)
    });
  },
  /**
   * @method dropCallback()
   * 
   *  scope notes:
   *   'this' = Panel object
   *   'drag' = Draggable object
   *   'drop' = Droppable object
   *    'evt' = Mouse Event object
   */
  dropCallback : function(drag, drop, evt) {
    if (drag.hasClassName('widget')) this.addPanel(drag, drop, evt, this);
  },
  hoverCallback : function(drag, drop, evt) {
    //this.element.style.backgroundColor = 'yellow';
  },
  
  
  /********************************************************************************************
   * Setters:
   *******************************************************************************************/
  setName : function(name) {
    this.options.name = name;
  },
  
  setPageID : function(id) {
    this.options.pageID = id;
  },
  
  setPanels : function(panels) {
    this.options.panels = panels;
  },
  
  
  /********************************************************************************************
   * add/remove/etc:
   *******************************************************************************************/
  addPanel : function(drag, drop, e, _this) {
    if(zip.debug) console.log("@ %s.addPanel(%o,%o,%o)",this.element.id,drag,drop,e);
    
    var _panel,
     dragOffsets = drag.positionedOffset(),
     pageOffsets = $('Page1').positionedOffset();//TODO: #Page1 will break, make more dynamic
     
    _panel = Object.extend(
      _this.panelTemplate(), 
      {coords:{
        x : dragOffsets.left - pageOffsets.left,
        y : dragOffsets.top - pageOffsets.top
      }}
    );
    
    this.Panels.push(new Zipzing.Panel({
      survey : _this.Survey,
      page   : _this,
      panel  : _panel
    }));
    //this.register('sortables');
  },
  
  panelTemplate : function() {
    var panelCount = this.Panels.length + 1;
    return {
      key       : 'panel-' + panelCount,//TODO: this id is *NOT* unique enough (will duplicate across pages)
      panelID   : panelCount,
      options   : {},
      behaviors : {},
      observers : {},
      coords    : {}
    };
  },
  
  removePanel: function(panel){
    // remove from array
    // ? panels.pop(panel);
    
    // remove from DOM
    panel.remove();
    
    // clean up object itself 
  },
  
  register : function(what) {
    if(zip.debug) console.log('@ %s.register( %s )', this.element.id, what);
    
    // Important note: To ensure that two way dragging between containers is possible, 
    //  place all Sortable.create calls after the container elements [are created].
    this.Panels.each(function(panel) {
      panel.register(what);
      /*panel.Rows.each(function(r) {
        if(zip.debug) console.log(" --> verifying %s.Panel: %s", r.element.id, r.Panel.element.id);
      });*/
    })
  },
  
  
  /********************************************************************************************
   * GUI Utilities & FX!
   *******************************************************************************************/
  slide : function(e) { // slide pages like a slideshow... SEE: http://www.panic.com/coda/
    var distance = 0 - this.element.getWidth();
    if(zip.debug) console.log("@ %s.slide(%o), distance=%s", this.element.id, e, distance);
    
    new Effect.Move(this.element,{
      x          : distance,
      y          : 0,
      duration   : 0.5,
      transition : Effect.Transitions.linear
    });
    //--this.element.style.zIndex;
  },
  
  paint : function() {
    if(zip.debug) console.log('invoked Zipzing.Page.paint() on ' + this.element.id);
    //new Effect.Appear(this.element,{duration:0.5,from:0.4});
  }
});
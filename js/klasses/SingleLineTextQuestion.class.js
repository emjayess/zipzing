/**
 * @Class SingleLineTextQuestion
 *  inherits from OpenEndedQuestion, which inherits from Question...
 *
 *  Question
 *    OpenEndedQuestion
 *      SingleLineTextQuestion
 */
Zipzing.SingleLineTextQuestion = Class.create(
 Zipzing.OpenEndedQuestion, {
  /********************************************************************************************
   * Constructor
   *******************************************************************************************/
  initialize : function($super, ctorData) {
    $super(ctorData);
    this.setInputSize(ctorData.inputsize || "20px");
    
    // SinglelineText Options
    /*
    this.options = Object.extend({
      confirmation_required : false
    }, ctorData.options || {});
    this.initOptions();
    */
    
    this.properties = {
      io        : Object.extend({/*none*/}, this.properties.io),
      hash      : Object.extend({/*none*/}, this.properties.hash),
      observers : this.properties.observers.concat([
        {elm:'singlelineConfirmYesNo', evt:'change', fn:this.setConfirmYesNo.bindAsEventListener(this)},
        {elm:'singlelineConfirmLabel', evt:'keyup',  fn:this.setConfirmLabel.bindAsEventListener(this)}
      ])
    };
  },
  
  /********************************************************************************************
   * Initialize helper(s)
   *******************************************************************************************/
  initConfirmDOMElement : function() {
    var DNA = $('DNA_Confirm');
    return $(Builder.node(DNA.tagName,{
      //id        : 'somethingUnique',
      className : 'confirm'
    })).update(DNA.innerHTML || DNA.innerText);
  },
  
  /*
  initOptions : function() {
    if (this.options.confirmation_required) this.setConfirmation(this.options.confirmation_required);
  },*/
  
  /********************************************************************************************
   * Setter(s)
   *******************************************************************************************/
  setInputSize : function(size){
    this.inputSize = size;
    //TODO: paint
    //this.element.down('input').style.width?
  },
  
  setConfirmYesNo : function(val) {
    if(zip.debug) console.log("@SingleLineText.setConfirmYesNo(%o)", arguments);
    //
  },
  
  setConfirmLabel : function(val) {
    if(zip.debug) console.log("@SingleLineText.setConfirmationLabel(%o)", arguments);
    //
  }
});
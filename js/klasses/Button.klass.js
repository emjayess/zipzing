Zipzing.Button = Class.create({

  initialize : function(ctorData){
    this.Row = ctorData.row;
    this.element = ctorData.element || this.initDOMElement(ctorData.button);
    this.Label = ctorData.label || "Submit";
    this.options = Object.extend({
      buttonID   : null,
      type       : "submit",
      label      : "Submit",
      bgcolor    : "ececec",
      isBeveled  : true,
      dimensions : new Zipzing.Dimensions({}),
      imageURL   : null,
      font       : new Zipzing.Font({})
    }, arguments[1] || {}/*ctorData.options*/);
    this.initOptions();

    this.behaviors = Object.extend({
      //resizable:false //(?)
    }, arguments[2] || {}/*ctorData.behaviors*/);
    this.paint();
    //this.initBehaviors();
  },

  initDOMElement : function(b){
    var DNA = $('DNA_Button');
    return $(Builder.node(DNA.tagName,{
      id        : b.buttonID,
      style     : 'display:none;',
      className : 'ZipzingButton'
    })).update(DNA.innerHTML || DNA.innerText);
  },

  initOptions : function(){
    if (this.options.buttonID) this.setButtonId(this.options.buttonID);
    if (this.options.type) this.setType(this.options.type);
    //if (this.options.label) this.setLabel(this.options.label);
    if (this.options.bgcolor) this.setBGColor(this.options.bgcolor);
    if (this.options.isBeveled) this.setIsBeveled(this.options.isBeveled);
    if (this.options.imageURL) this.setImageURL(this.options.imageURL);
    if (this.options.font) this.setFont(this.options.font);
    //
    if (this.Label) this.setLabel(this.Label);
  },

  setButtonId : function(id){
    this.options.buttonID = id;
  },

  setType : function(type){
    this.options.type = type;
  },

  setLabel : function(lbl){
    this.Label = lbl;
    // paint
    this.element.down('input').value = lbl;
  },

  setBGColor : function(bgcolor){
    this.options.bgcolor = bgcolor;
  },

  setIsBeveled : function(isBeveled){
    this.options.isBeveled = isBeveled;
  },

  setImageURL : function(url){
    this.options.imageURL = url;
  },

  setFont : function(font){
    //@@@@@todo
  },

  paint : function(){
    this.Row.element.insert(this.element);
    this.Row.autosize(this.element.getHeight());
    new Effect.Appear(this.element, this.behaviors.FxDuration);
  },

  lock : function(){
    //@@@@@todo
  },

  unlock : function(){
    //@@@@@todo
  }
});
Zipzing.QuestionAnswer = Class.create({
  
  /********************************************************************************************
   * Constructor
   *******************************************************************************************/
  initialize : function(ctorData) {
    this.element = ctorData.element || this.initDOMElement(ctorData.answer);
    
    // QuestionAnswer Options
    this.setLabel(ctorData.label || "random answer...");
    this.setValue(ctorData.value || "random value");
    this.setScore(ctorData.score || 0);
  },
  
  
  /********************************************************************************************
   * initialize() Helpers
   *******************************************************************************************/
  initDOMElement : function(a) {
    // Builder.node ...
  },
  
  /********************************************************************************************
   * Setters
   *******************************************************************************************/
  setLabel : function(lbl) {
    this.Label = lbl;
    // paint...
    if (this.element && this.element.down('label')) this.element.down('label').update(lbl);
  },
  
  setValue : function(value) {
    this.Value = value;
    // setAttribute...
  },
  
  setScore : function(score) {
    this.Score = score;
    // database only...
  }
});